# README #

This is a small python package that provides an upload interface for the Zenodo API.

## Basic structure ##

The Zenodo upload API is based on a 'deposition': a set of metadata describing a set of one or more files. In order to submit a deposition through the API, a user needs three things:

1. An authorisation token, which can be obtained from the Zenodo user profile settings.
2. The set of files to be uploaded.
3. A JSON object describing the metadata for the new deposition.

The first two are easy, but the metadata is more tricky. 

## Metadata creation ##

The metadata structure is described here: [https://zenodo.org/dev]. It needs to be submitted as JSON data and contains three basic types of object:

1. Simple string data: ``{"title":"My deposition"}``
2. Arrays of strings: ``{"keywords": ["Data", "Other goodness"]}``
3. Arrays of objects: ``{"creators": [{"name":"Rincewind","affiliation":"Unseen University"}, {"name":"Librarian","affiliation":"Unseen University"}]}`` 

In order to avoid users having to edit JSON files, the code here takes input from a flat text template. Since none of the object arrays have more nested structure than ``parent.child`` this is relatively easy. 

|         |              |                |
|---------|--------------|----------------|
|title    |My deposition |                |
|keywords |Data          |Other goodness  |
|creators.name |Rincewind  |Librarian     |
|creators.affiliation |Unseen University  |Unseen University     |


