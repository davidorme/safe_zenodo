from unittest import TestCase

import safe_zenodo

# TODO - this code is just the things to check at the moment,
# and nothing is built in to actual unit test functions.


# test for missing files
payload = zenodo_payload_validation('testdata/payload_missing_files.txt')

# test for controlled vocabularies
payload = zenodo_payload_validation('testdata/payload_vocab_errors.txt')

# simple test of required field failure
payload = zenodo_payload_validation('testdata/payload_missing_required_fields.txt')

# simple test of valid set
payload = zenodo_payload_validation('testdata/payload_valid.txt')


auth_json = '../auth/sandbox_token.json'

payload_file = 'testdata/payload_valid.txt'
zenodo_upload(payload_file, auth_json, sandbox=True)

payload_file = 'testdata/payload_valid_embargoed.txt'
zenodo_upload(payload_file, auth_json, sandbox=True)




auth_json = '/Users/dorme/Research/SAFE/Zenodo/auth/zenodo_token.json'
payload_file = '/Users/dorme/Research/SAFE/Zenodo/payloads/SAFE_mammal_traps.txt'


payload = zenodo_payload_validation(payload_file)
zenodo_upload(payload_file, auth_json)
