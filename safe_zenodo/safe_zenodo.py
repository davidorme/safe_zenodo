#!/usr/bin/python

import json
import requests
import sys
import os

# this is not a standard package but is needed to monitor file uploads
from requests_toolbelt import MultipartEncoder, MultipartEncoderMonitor


# Functions to submit to the Zenodo API

# Architecture notes
# - The basic structure is that a Zenodo deposition is a bunch of 
#   metadata with a set (1 or more) attached files.
# - So a submit function needs to read in a payload of metadata and data file(s)
# - Note that metadata is for a set of files (whole deposition) not individual files.
# - This is more complex than you'd want as arguments, so need a payload file
# - The payload file needs to be error checked against the metadata standard
# - Once checked, the payload can be sent up to Zenodo 
# - The deposition data then needs to be tied back to the payload, so some kind of 
#   record of what went where and when.

# METADATA PROVISION
# - JSON files are a bugger to manually edit
# - Could provide a default user JSON template and modify
#   via the command line (override defaults) but actually 
#   having a file per deposition is a better record.
# - So a simple text template file with tags and values and assemble JSON in code

# METADATA VALIDATION
# - Could let the server handle metadata validation?
# - No mechanism that doesn't also create a deposition when valid.

def read_zenodo_licence_vocab():
    
    # grabs a licence vocab from zenodo, checking whether
    # other vocabs are available, as it would be better to
    # grab the validation live rather than hardcode it in 
    # a function, which is fragile to API changes.
    
    url = "https://zenodo.org/kb/export?kbname=licenses&format=json"
    response = requests.get(url)
    licences_vocab = set([x['value'] for x in response.json()])
    
    return licences_vocab


def zenodo_controlled_vocabs():
    
    # currently just provides a hard coded JSON definition,
    # of the accepted values for 'tag' and 'tag.array_object'
    
    vocabs = '''{"upload_type": ["publication","poster","presentation","dataset",
                                   "image","video","software"],
              "publication_type": ["book", "section", "conferencepaper", "article", 
                                   "patent", "preprint", "report", "softwaredocumentation", 
                                   "thesis", "technicalnote", "workingpaper", "other"],
              "image_type": ["figure", "plot", "drawing", "diagram", "photo", "other"],
                                   "access_right": ["open", "embargoed", "restricted", "closed"],
              "licence": ["geo-no-fee-unrestricted", "ver2_eiffel", "gpl-2.0", "ukclickusepsi",
                                   "tcl", "other-closed", "bsl1.0", "ipafont", "met-office-cp", "lgpl-3.0", 
                                   "against-drm", "multics", "eudatagrid", "agpl-v3", "real", "frameworx",
                                   "attribution", "nokia", "apsl-2.0", "lucent-plan9", "hesa-withrights", 
                                   "fal", "other-at", "ntp-license", "cuaoffice", "isc-license", "intel-osl",
                                   "ecl2", "opengroup", "lucent1.02", "cc-nc", "geogratis", "fair", "ms-rl", 
                                   "osl-3.0", "gpl-3.0", "ricohpl", "odc-by", "motosoto", "qtpl", "eclipse-1.0",
                                   "pythonpl", "other-open", "dsl", "ukpsi", "apache2.0", "uoi-ncsa", "oclc2",
                                   "mozilla1.1", "odc-pddl", "canada-crown", "xnet", "apache", 
                                   "pythonsoftfoundation", "postgresql", "cc-by-sa", "cc-by-nc-sa-4.0",
                                   "other-pd", "bsd-2-clause", "ca-tosl1.1", "rpl1.5", "sleepycat", "ms-pl", 
                                   "nasa1.3", "cc-by-nc-4.0", "naumen", "afl-3.0", "uk-ogl", "cc-by-nc-nd-4.0",
                                   "cddl1", "w3c", "zpl", "dli-model-use", "eupl-1.1", "notspecified", "nosl3.0",
                                   "openfont", "mozilla", "zlib-license", "mitre", "localauth-withrights",
                                   "mit-license", "entessa", "vovidapl", "user-jsim", "wxwindows", "lgpl-2.1",
                                   "gfdl", "cc-by-nd-4.0", "jabber-osl", "cc-by", "odc-odbl", "simpl-2.0", "ukcrown-withrights",
                                   "artistic-license-2.0", "other-nc", "sunpublic", "cpal_1.0", "cc-zero", "ibmpl",
                                   "sybase", "bsd-3-clause", "bsd-license", "sun-issl", "eiffel", "php", "ukcrown",
                                   "eurofound", "miros", "apl1.0", "nethack"],
              "related_identifiers.relation": ["isCitedBy", "cites", "isSupplementTo", "isSupplementedBy", 
                                    "isNewVersionOf", "isPreviousVersionOf", "isPartOf", "hasPart", "compiles", 
                                    "isCompiledBy", "isIdenticalTo", "isAlternateIdentifier"],
              "contributors.type": ["ContactPerson","DataCollector","DataCurator","DataManager","Editor",
                                    "Researcher","RightsHolder","Sponsor","Other"]}'''
    
    return json.loads(vocabs)

# function to convert multiple tags in the payload data describing 
# an array of objects into the correct format

def object_array_extract(payload_data, parent):
    
    # get tags starting with "parent." 
    parent_length = len(parent) + 1
    matching_tags = [x for x in payload_data.keys() if x[:parent_length] == parent + '.']
    
    # if there are no matches, don't do anything and exit
    # - this is not necessarily an error - the user might
    #   be submitting a short but valid subset (e.g. no thesis_supervisors)
    if len(matching_tags) == 0:
        sys.stdout.write("'{}' parent tags not located.\n".format(parent))
        return
    
    # pop that information out of the payload data
    # so that the information is removed from the calling object
    child_metadata = [payload_data.pop(x) for x in matching_tags]
    child_tags = [x[parent_length:] for x in  matching_tags]
    
    # if there isn't any data here at all, this has a list with a single empty list
    # so just return an empty list
    if child_metadata == [[]]:
        return []
    
    # convert into an array of dictionaries
    # - when there are multuple fields, use map to match up uneven lengths
    #   from possible empty members (e.g. not everyone has an orcid)
    if len(child_metadata) > 1:
        child_array = map(None, *child_metadata)
    else: 
        child_array = child_metadata
    # map fills in gaps with None which then needs converting to empty strings
    child_array = [['' if x is None else x for x in arr] for arr in child_array]
    
    # - now add tag names and return the array
    child_array = [dict(zip(child_tags, arr)) for arr in child_array]
    
    return child_array


def zenodo_payload_validation(payload_file, max_size=(2 * 1000 ** 3)):
    
    '''
    This is the workhorse function to load and check a payload file.
    
    The basic parts are:
    (A) Load the data from the tab delimited text file 
    (B) Check payload files exist and are within size limits
    (C) Convert the metadata into a dictionary 
    (D) Check the inputs against controlled vocabularies
    (E) Convert rows that represent object arrays in the Zenodo API
        into those arrays
    (F) Run checks on required fields
    
    Provided the file is found the function runs through all steps
    reporting on errors along the way
    '''
    
    # Is everything ok
    all_ok = True
    
    # (A) open the payload, with cross platform line endings
    # and load the data
    try:
        infile = open(payload_file, 'rU')
    except:
        sys.stderr.write('Could not open {}\n'.format(payload_file))
        raise IOError
    else:
        base = os.path.basename(payload_file)
        payload_data = infile.readlines()
        infile.close()
        sys.stdout.write('Loaded {}\n'.format(base))
    
    # NOTE - the next two steps strip all white space, giving uneven
    # length lines and then x[1:] converts empty lines to empty lists.
    # This is needed to handle array objects and gets cleaned up along the way
    
    # clean the whitespace from the ends and split
    payload_data = [x.strip().split('\t') for x in payload_data]
    
    # get tags and values
    tags = [x[0] for x in payload_data]
    values = [x[1:] for x in payload_data]
    
    # (B) extract files now as the file tag can be duplicated 
    sys.stdout.write('[ ] Checking upload files\n')
    files = [vl for tg, vl in zip(tags, values) if tg == 'file']
    
    # are there actually rows with file names?
    if files == []:
        sys.stderr.write(' - No files specified for upload.\n')
        all_ok = False
    
    # Check they are really existing, single filenames with sensible sizes
    checked_files = []
    
    for each_file in files:
        # single file?
        if len(each_file) > 1:
            sys.stderr.write(' - Multiple upload files specified on a single line.\n')
            all_ok = False
        else:
            curr_fullname = each_file[0]
            curr_basename = os.path.basename(curr_fullname)
        
        # file exists (and is a file, not a directory)
        if not os.path.isfile(curr_fullname):
            sys.stderr.write('- File not found: {}\n'.format(curr_basename))
            all_ok = False
        else:
            # file is under the maximum file size
            file_size = os.path.getsize(curr_fullname)
            if file_size > max_size:
                sys.stderr.write(' - File oversize at {} bytes: {}\n'.format(file_size, curr_basename))
                all_ok = False
            else:
                sys.stdout.write(' + Found file to upload ({} bytes): {}\n'.format(file_size, curr_basename))
                checked_files.append({'path': curr_fullname, 'size': file_size, 'name': curr_basename})
    
    # (C) convert to a dictionary and strip out 'file'
    payload_data = dict(zip(tags, values))
    del payload_data['file']
    
    # (D) check controlled vocabularies - this operates on
    #     whole lists so isn't sensitive to array versus string metadata types
    sys.stdout.write('[ ] Checking controlled vocabularies\n')
    
    vocabs = zenodo_controlled_vocabs()
    for tag in payload_data:
        values = payload_data[tag]
        # if this tag has a controlled vocab is it being obeyed? 
        if tag in vocabs.keys() and len(values) > 0:
            for v in values:
                if v not in vocabs[tag]:
                    error = " - Error: '{}' not a valid value for tag '{}'\n"
                    sys.stderr.write(error.format(v, tag))
                    all_ok = False
    
    # (E) convert array types into arrays
    #     From now on, we are checking metadata, so pass processed payload data
    #     into a new clean metadata object.
    sys.stdout.write('[ ] Compiling metadata structure\n')
    metadata = {}
    
    # - object arrays converted in place in payload_data object
    object_array_fields = ['creators', 'contributors', 'related_identifiers', 'communities', 
                           'grants','thesis_supervisors', 'subjects']
    
    for fld in object_array_fields:
        metadata[fld] = object_array_extract(payload_data, fld)
    
    # - copy string arrays directly
    string_array_fields = ['keywords', 'references']
        
    for fld in string_array_fields:
        metadata[fld] = payload_data.pop(fld)
    
    # - copy everything that is left over as straight strings 
    for k, v in payload_data.iteritems():
        n_vals = len(v)
        if n_vals > 1:
            sys.stderr.write(" - Multiple values provided for field '{}'\n".format(k))
            all_ok = False
        elif n_vals == 0:
            metadata[k] = ''
        else:
            metadata[k] = v[0]
    
    # (F) check required fields 
    sys.stdout.write('[ ] Checking required fields\n')
    
    # Fields that cannot be empty
    empty_values = [[], ''] # empty list or empty string
    not_empty = ['upload_type', 'title','creators','description','access_right']
    
    for field in not_empty:
        if metadata[field] in empty_values:
            sys.stderr.write(" - '{}' cannot be blank.\n".format(field))
            all_ok = False
    
    # conditional tests
    # - sets of {'fld': 'check_field','vals':['check_values'],'req': 'field_that_must_be_completed'}
    conditions = [{'fld':'upload_type','vals':['publication'],'req': 'publication_type'},
                  {'fld':'upload_type','vals':['image'],'req': 'image_type'},
                  {'fld':'access_right','vals':['open', 'embargoed'],'req': 'license'},
                  {'fld':'access_right','vals':['embargoed'],'req': 'embargo_date'},
                  {'fld':'access_right','vals':['restricted'],'req': 'access_conditions'}]
    
    # check those sets of conditions
    for c in conditions:
        if metadata[c['fld']] in c['vals'] and metadata[c['req']] in empty_values:
            error_msg = " - {} cannot be blank when {} is '{}'.\n".format(c['req'],
                         c['fld'], metadata[c['fld']])
            sys.stderr.write(error_msg)
            all_ok = False
    
    # strip out empty strings and arrays to minimize traffic
    for k in metadata.keys():
        if metadata[k] in empty_values:
            del metadata[k]
    
    # were any errors encountered?
    if all_ok:
        return {'metadata': metadata, 'files': checked_files}
    else:
        return None


def api_call_success(response):
    
    '''
    This function checks to see if the API request has been successful 
    and formats an error message if not. The code is basically the same for
    all API events, so this is reusable
    
    It either prints a formatted error message and exits or a success message.
    '''
    
    # Look for success codes
    if response.status_code not in [200, 201,202, 204]:
        
        sys.stderr.write(response.status_code)
        # get the error information
        err_info = response.json()
        
        # build the error message 
        err_msg = "Error code {}: {}\n".format(err_info['status'], err_info['message'])
        if err_info.has_key('errors'):
            for this_error in err_info['errors']:
                if this_error.has_key('field'):
                    err_msg += "Zenodo error {}: {} for field {}.\n".format(this_error['code'], 
                                this_error['message'], this_error['field'])
                else:
                    err_msg += "Zenodo error {}: {}.\n".format(this_error['code'], this_error['message'])
        
        return False, err_msg
    else:
        return True, ''


# Function to create a callback function to monitor 
# upload progress on a file

def create_callback(encoder, file_name, file_size):
    
    # I'll be honest, some of this is cargo cult programming
    # but figuring out how to pass additional arguments into 
    # callback to get a simple progress bar is confusing
    # see:
    # https://gitlab.com/sigmavirus24/toolbelt/blob/master/examples/monitor/progress_bar.py
    
    file_name = file_name
    file_size = file_size
    
    def callback(monitor):
        n = ((monitor.bytes_read * 100 / file_size) / 5)
        progress = '>' * (n) + ' ' * (20 - n)
        sys.stdout.write('{} :Uploading {}\r'.format(progress, file_name))
	sys.stdout.flush()
    
    return callback


def zenodo_upload(payload_file, auth_json, json_out=False, sandbox=False, progress_bar=True):

    '''
    This function takes a payload_file and checks it
    and then uploads the payload to Zenodo using the
    authorisation token provided in the auth_json file.
    '''
    
    # check and get the payload files and metadata
    payload = zenodo_payload_validation(payload_file)
    if payload is None:
        sys.stderr.write('Errors in payload file.\n')
        raise IOError
    else:
        metadata = payload['metadata']
        files = payload['files']
    
    # load access token: just {"access_token": "token_value"}
    try:
        auth_file = open(auth_json, 'rU')
        auth_params = json.load(auth_file)
        auth_file.close()
    except:
        sys.stderr.write('Could not load authorisation token from file.\n')
        raise IOError
    
    if 'access_token' not in auth_params.keys():
        sys.stderr.write("'access_token' field not found in authorisation file.\n")
        raise ValueError
    
    # set of base URLS for API
    if sandbox:
        api_base = 'https://sandbox.zenodo.org/api/'
    else:
        api_base = 'https://zenodo.org/api/'
    
    depo_base = api_base + "deposit/depositions/"
    depo_instance = api_base + "deposit/depositions/{depo_id}"
    depo_files = api_base + "deposit/depositions/{depo_id}/files"
    depo_publish = api_base + "deposit/depositions/{depo_id}/actions/publish"

    # create deposition - POST a set of deposition metadata to
    #                     the depositions URL. Can also POST null
    #                     metadata '{}' to create the depo and then
    #                     PUT to add metadata to the deposition
    headers = {"Content-Type": "application/json"}
    metadata = json.dumps({'metadata': metadata})
    depo_create = requests.post(depo_base, data=metadata, headers=headers, params=auth_params)
    
    # and check for success
    depo_success, message = api_call_success(depo_create)
    if depo_success: 
        sys.stdout.write('Zenodo deposition created\n')
        depo_id = depo_create.json()['id']
    else:
        sys.stdout.write('Failed to create Zenodo deposition\n' + message)
        raise RuntimeError
    
    # add files to that deposition
    # - TODO currently doing one by one and can pass an array
    #        but does this permit finer error logging
    for f in files:
        
        # NOTE that the server has to calculate and return a checksum, which I
        # think causes the pause between the file uploading and the request returning
        if progress_bar:
            # gather the name and data and post using MultipartEncoder because
            # this has MultipartEncoderMonitor which enables a progress bar
            to_upload = MultipartEncoder(fields={'file': (f['name'], open(f['path'], 'rb'))})
            callback = create_callback(to_upload, f['name'], f['size'])
            monitor = MultipartEncoderMonitor(to_upload, callback)
            
            file_upload = requests.post(depo_files.format(depo_id=depo_id), 
                                        data=monitor, params=auth_params,
                                        headers={'Content-Type': monitor.content_type})
        else:
            sys.stdout.write('Uploading {}'.format(f['name']))
            # use the standard example from the API quick start as a block
            data = {'filename': f['name']}
            the_file = {'file': open(f['path'], 'rb')}
            file_upload = requests.post(depo_files.format(depo_id=depo_id), data=data, 
                                        files=the_file, params=auth_params)
        
        # check for success
        upload_success, message = api_call_success(file_upload)
        if upload_success: 
            sys.stdout.write('\nFile uploaded: {}\n'.format(f['name']))
        else:
            sys.stdout.write('\nFailed to upload: {}\n'.format(f['name']) + message)
            # TODO roll back the deposition
            raise RuntimeError
    
    # and now publish the deposition to make it publically visible through the record API
    publish = requests.post(depo_publish.format(depo_id=depo_id), params=auth_params)

    publish_success, message = api_call_success(publish)
    if publish_success: 
        sys.stdout.write('Deposition published.\n')
    else:
        sys.stdout.write('Failed to publish deposition\n' + message)
        # TODO roll back the deposition
        raise RuntimeError
    
    # Output a json file of the record - json_out could be True or a pathname
    if json_out or isinstance(json_out, str):
        if json_out == True:
            json_out = os.path.splitext(payload_file)[0] + '.json'
        try:
            outfile = open(json_out, 'w')
            json.dump(publish.json(), outfile)
            outfile.close()
        except:
            sys.stderr.write('Failed to write JSON for published deposition to file.\n')
            print json.dumps(publish.json())
            raise IOError
    else:
        print json.dumps(publish.json())



# Unused code for listing depositions and files, to work into another function.
#
# # list files in a deposition
# r = requests.get(depo_files.format(depo_id=depo_id), params=auth_params)
#
# # update existing deposition (what about multiple files)
#
# url = "https://zenodo.org/api/deposit/depositions/1234?access_token=ACCESS_TOKEN"
# headers = {"Content-Type": "application/json"}
# data = {"metadata": {"title": "My first upload", "upload_type": "poster", "description": "This is my first upload", "creators": [{"name": "Doe, John", "affiliation": "Zenodo"}]}}
#
# r = requests.put(url, data=json.dumps(data), headers=headers)
#
#
# # list deposition files
#
# r = requests.get("https://zenodo.org/api/deposit/depositions/1234/files?access_token=ACCESS_TOKEN")
#
# # list depositions
# rlist = requests.get(depo_base, params=auth_params)
#
# # return values
# print r.json()
# r.status_code
# r.headers
# r.url
#
#


