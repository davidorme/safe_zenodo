from setuptools import setup

setup(name='safe_zenodo',
      version='0.1',
      description='Functions to use the Zenodo REST API, developed for use by the SAFE project',
      url='https://bitbucket.org/davidorme/safe_zenodo',
      author='David Orme',
      author_email='d.orme@imperial.ac.uk',
      license='MIT',
      packages=['safe_zenodo'],
      package_data={'safe_zenodo': ['tests/data/*.txt','payload_template.txt']},
      entry_points = {
              'console_scripts': ['zenodo_upload=safe_zenodo.command_line:zenodo_upload_cli',
                                  'zenodo_template=safe_zenodo.command_line:zenodo_new_template_cli',
                                  'zenodo_badge_syntax=safe_zenodo.command_line:zenodo_badge_syntax_cli']
      },
      zip_safe=False)
